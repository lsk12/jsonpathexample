//
//  SimpleTestsTests.swift
//  SimpleTestsTests
//
//  Created by Laskowski, Michal on 10/01/2019.
//  Copyright © 2019 Viacom. All rights reserved.
//

import XCTest
@testable import SimpleTests

class SimpleTestsTests: XCTestCase {

    let testDictionary: [String: Any] = [
        "countries": [
            [
                "name": "japan",
                "capital": [
                    "name": "tokyo",
                    "lat": "35.6895",
                    "lon": "139.6917"
                ],
                "language": "japanese"
            ],
            [
                "name": "poland",
                "capital": [
                    "name": "warszawa",
                    "lat": "35.6895",
                    "lon": "139.6917"
                ],
                "language": "polish",
                "regions": [
                    "mazowsze": [
                        "majorCity": "Warszawa"
                    ],
                    "malopolska": [
                        "majorCity": "Warszawa"
                    ]
                ]
            ]
        ],
        "airports": [
            "germany": ["FRA", "MUC", "HAM", "TXL"]
        ]
    ]

    func testKeyPathIsDecoded() {
        let keypath = try! [JsonPathElement].init(jsonPathString: "countries[?(@.language == 'polish')].capital")

        XCTAssertEqual(keypath.count, 2)
        XCTAssertEqual(keypath[0], JsonPathElement.child(array: "countries", containingKey: "language", value: "polish"))
        XCTAssertEqual(keypath[1], JsonPathElement.key("capital"))
    }


    func testUpdatesValuesByFilter() {
        var dict = testDictionary
        let keypath = try! [JsonPathElement].init(jsonPathString: "countries[?(@.language == 'polish')].capital")
        dict.setValue("abc", forKeyPath: keypath)
        XCTAssertEqual((dict["countries"] as! [[String: Any]])[1]["capital"] as! String, "abc")
    }

    func testUpdatesValuesByIndex() {
        var dict = testDictionary
        let newValue: [String: Int] = ["test": 42]
        let keypath = try! [JsonPathElement].init(jsonPathString: "countries[1].language")
        dict.setValue(newValue, forKeyPath: keypath)
        XCTAssertEqual(((dict["countries"] as! [[String: Any]])[1]["language"] as! [String: Int])["test"], 42)
    }
}
