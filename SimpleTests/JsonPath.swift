//
//  JsonPath.swift
//
//  Created by Laskowski, Michal on 10/01/2019.
//  Copyright © 2019 Viacom. All rights reserved.
//

import Foundation

fileprivate extension String {
    var wholeRange: NSRange {
        return NSRange(location: 0, length: self.utf16.count)
    }

    var arrayWithIndex: (String, Int)? {
        let regex = try! NSRegularExpression(pattern: "^([a-zA-Z]+)\\[(\\d+)\\]$")
        let matches = regex.matches(in: self, options: [], range: wholeRange)
        guard let match = matches.first, matches.count == 1, match.numberOfRanges == 3 else {
            return nil
        }

        let indexAsString = String(self[Range(match.range(at: 2), in: self)!])
        guard let index = Int(indexAsString) else {
            return nil
        }
        let arrayName = String(self[Range(match.range(at: 1), in: self)!])

        return (arrayName, index)
    }

    var isValidKeyType: Bool {
        let regex = try! NSRegularExpression(pattern: "^([a-zA-Z]+)$")
        let matches = regex.matches(in: self, options: [], range: wholeRange)
        return matches.count == 1
    }

    var arrayObjectWithKeyAndValue: (String, String, String)? {
        let regex = try! NSRegularExpression(pattern: "^([a-zA-Z]+)\\[\\?\\(\\@\\.(\\S+) == ['\"]?(.*?)['\"]?\\)]$")
        let matches = regex.matches(in: self, options: [], range: wholeRange)
        guard let match = matches.first, matches.count == 1, match.numberOfRanges == 4 else {
            return nil
        }

        let arrayName = String(self[Range(match.range(at: 1), in: self)!])
        let key = String(self[Range(match.range(at: 2), in: self)!])
        let value = String(self[Range(match.range(at: 3), in: self)!])

        return (arrayName, key, value)
    }
}

enum JsonPathElement {
    case key(String)
    case arrayIndex(array: String, position: Int)
    case child(array: String, containingKey: String, value: String)
}

extension JsonPathElement: Equatable {}

enum JSONPathError: Error {
    case badJsonPathString
}

// do not split by dot if preceded by @ (used for filtering)
private let jsonPathSplitRegex = try! NSRegularExpression(pattern: "[^\\@](\\.)")

private extension String {

    func splitJsonPathComponents() -> [String] {
        let matches = jsonPathSplitRegex.matches(in: self, options: [], range: wholeRange)
        let splitPositions = matches.map { (match) -> Int in
            match.range.location + 1
        }

        var lastPosition = 0
        var components = [String]()
        splitPositions.forEach { (position) in
            let length = position - lastPosition
            guard let range = Range(NSRange(location: lastPosition, length: length), in: self) else {
                return
            }
            let component = String(self[range])
            components.append(component)
            lastPosition = position + 1
        }

        if let lastRange = Range(NSRange(location: lastPosition, length: count - lastPosition), in: self) {
            let lastComponent = String(self[lastRange])
            components.append(lastComponent)
        }

        return components
    }
}

extension Array where Element == JsonPathElement {
    init(jsonPathString: String) throws {
        let components = jsonPathString.splitJsonPathComponents()
        self = try components
            .map { comp -> JsonPathElement in
                if let (arrayName, arrayIndex) = comp.arrayWithIndex {
                    return .arrayIndex(array: arrayName, position: arrayIndex)
                } else if let (arrayName, key, value) = comp.arrayObjectWithKeyAndValue {
                    return .child(array: arrayName, containingKey: key, value: value)
                } else if comp.isValidKeyType {
                    return .key(comp)
                } else {
                    throw JSONPathError.badJsonPathString
                }
        }
    }
}

extension Dictionary where Key == String {
    // recursively (attempt to) access the queried subdictionaries to
    // finally replace the "inner value", given that the key path is valid
    mutating func setValue(_ value: Any, forKeyPath keyPath: [JsonPathElement]) {
        guard let path = keyPath.first else { return }

        let keyPathWithoutCurrent = Array(keyPath.dropFirst())
        switch path {
        case .arrayIndex(let arrayName, let index):
            guard var subArray = self[arrayName] as? [[Key: Value]] else {
                return
            }
            subArray[index].setValue(value, forKeyPath: keyPathWithoutCurrent)
            (subArray as? Value).map { self[arrayName] = $0 }

        case .child(let arrayName, let key, let matchingValue):
            guard var subArray = self[arrayName] as? [[Key: Value]] else {
                return
            }
            
            for index in 0..<subArray.count {
                guard subArray[index][key] as? String == matchingValue else {
                    continue
                }
                subArray[index].setValue(value, forKeyPath: keyPathWithoutCurrent)
                (subArray as? Value).map { self[arrayName] = $0 }
            }

        case .key(let key):
            if keyPath.count == 1 {
                (value as? Value).map { self[key] = $0 }
            } else if var subDict = self[key] as? [Key: Value] {
                subDict.setValue(value, forKeyPath: keyPathWithoutCurrent)
                (subDict as? Value).map { self[key] = $0 }
            }
        }
    }
}
